<?php

/**
 * @file
 * Post update functions for RNG.
 */

use Drupal\Core\Entity\EntityStorageException;

/**
 * The number of entities to process in each batch.
 *
 * @var int
 */
const RNG_UPDATE_BATCH_SIZE = 50;

/**
 * Resave all registrants to populate event links.
 *
 * Inspiration for this code was taken from
 * \Drupal\Core\Config\Entity\ConfigEntityUpdater::update().
 */
function rng_post_update_resave_registrants(&$sandbox = NULL) {
  $storage = \Drupal::entityTypeManager()->getStorage('registrant');

  if (!isset($sandbox['registrant_update'])) {
    $sandbox['registrant_update']['entities'] = $storage->getQuery()->accessCheck(FALSE)->execute();
    $sandbox['registrant_update']['count'] = count($sandbox['registrant_update']['entities']);
    $sandbox['registrant_update']['failures'] = [];
  }

  /** @var \Drupal\rng\Entity\RegistrantInterface $registrant */
  $registrants = $storage->loadMultiple(array_splice($sandbox['registrant_update']['entities'], 0, RNG_UPDATE_BATCH_SIZE));
  foreach ($registrants as $registrant) {
    try {
      $registrant->save();
    }
    catch (EntityStorageException $e) {
      $sandbox['registrant_update']['failures'][] = $registrant->id();
    }
  }

  $sandbox['#finished'] = empty($sandbox['registrant_update']['entities']) ? 1 : ($sandbox['registrant_update']['count'] - count($sandbox['registrant_update']['entities'])) / $sandbox['registrant_update']['count'];

  if ($sandbox['#finished'] === 1 && !empty($sandbox['registrant_update']['failures'])) {
    return t('Some registrants could not be resaved: @registrant_ids.', [
      '@registrant_ids' => implode(', ', $sandbox['registrant_update']['failures']),
    ]);
  }
}
