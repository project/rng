<?php

namespace Drupal\Tests\rng\Kernel;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\rng\EventManagerInterface;
use Drupal\rng\EventMetaInterface;
use Drupal\rng\Entity\RegistrantInterface;
use Drupal\rng\Entity\RegistrationInterface;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Tests the event meta class.
 *
 * @group rng
 * @coversDefaultClass \Drupal\rng\EventMeta
 */
class RngEventMetaTest extends RngKernelTestBase {

  use UserCreationTrait {
    createUser as drupalCreateUser;
  }

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['system', 'entity_test'];

  /**
   * The RNG event manager.
   *
   * @var \Drupal\rng\EventManagerInterface
   */
  protected $eventManager;

  /**
   * An event type for testing.
   *
   * @var \Drupal\rng\Entity\EventTypeInterface
   */
  protected $eventType;

  /**
   * Constant representing unlimited.
   *
   * @var int
   */
  protected $unlimited;

  /**
   * An config entity of type "registration_type".
   *
   * @var \Drupal\rng\RegistrationTypeInterface
   */
  protected $registrationType;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('entity_test');
    $this->installEntitySchema('user');
    $this->setupRngRules();

    $this->eventManager = $this->container->get('rng.event_manager');
    $this->eventType = $this->setupEventType();
    $this->unlimited = EventMetaInterface::CAPACITY_UNLIMITED;
    $this->registrationType = $this->createRegistrationType();
  }

  /**
   * Tests getting an event.
   *
   * @covers ::getEvent
   */
  public function testGetEvent() {
    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertSame($event, $event_meta->getEvent());
  }

  /**
   * Tests getting event type.
   *
   * @covers ::getEventType
   */
  public function testGetEventType() {
    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertSame($this->eventType->id(), $event_meta->getEventType()->id());
  }

  /**
   * Tests return value for accepting new registrations.
   *
   * @covers ::isAcceptingRegistrations
   * @dataProvider acceptRegistrationsDataProvider
   */
  public function testIsAcceptingRegistrations($expected, $values) {
    $event = $this->createEvent($values);
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertSame($expected, $event_meta->isAcceptingRegistrations());
  }

  /**
   * Data provider for ::testIsAcceptingRegistrations().
   *
   * @return array
   *   Test cases.
   */
  public function acceptRegistrationsDataProvider(): array {
    return [
      'status-enabled' => [
        'expected' => TRUE,
        'values' => [EventManagerInterface::FIELD_STATUS => TRUE],
      ],
      'status-disabled' => [
        'expected' => FALSE,
        'values' => [EventManagerInterface::FIELD_STATUS => FALSE],
      ],
    ];
  }

  /**
   * Tests return value for accepting new registrations if there is no field.
   *
   * @covers ::isAcceptingRegistrations
   */
  public function testIsAcceptingRegistrationsNoField() {
    $field = FieldConfig::loadByName('entity_test', 'entity_test', EventManagerInterface::FIELD_STATUS);
    $field->delete();

    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertTrue($event_meta->isAcceptingRegistrations());
  }

  /**
   * Tests returning the email address.
   *
   * @covers ::getReplyTo
   */
  public function testGetReplyTo() {
    $event = $this->createEvent([
      EventManagerInterface::FIELD_EMAIL_REPLY_TO => 'person@example.com',
    ]);
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertSame('person@example.com', $event_meta->getReplyTo());
  }

  /**
   * Tests returning the email address when the field does not exist.
   *
   * @covers ::getReplyTo
   */
  public function testGetReplyToNoField() {
    $field = FieldConfig::loadByName('entity_test', 'entity_test', EventManagerInterface::FIELD_EMAIL_REPLY_TO);
    $field->delete();

    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertNull($event_meta->getReplyTo());
  }

  /**
   * Tests if registrant may register more than once.
   *
   * @covers ::duplicateRegistrantsAllowed
   * @dataProvider duplicateRegistrantsAllowedDataProvider
   */
  public function testDuplicateRegistrantsAllowed($expected, $values) {
    $event = $this->createEvent($values);
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertSame($expected, $event_meta->duplicateRegistrantsAllowed());
  }

  /**
   * Data provider for ::testDuplicateRegistrantsAllowed().
   *
   * @return array
   *   Test cases.
   */
  public function duplicateRegistrantsAllowedDataProvider(): array {
    return [
      'enabled' => [
        'expected' => TRUE,
        'values' => [EventManagerInterface::FIELD_ALLOW_DUPLICATE_REGISTRANTS => TRUE],
      ],
      'disabled' => [
        'expected' => FALSE,
        'values' => [EventManagerInterface::FIELD_ALLOW_DUPLICATE_REGISTRANTS => FALSE],
      ],
    ];
  }

  /**
   * Tests duplicate registrants allowed when the field does not exist.
   *
   * @covers ::duplicateRegistrantsAllowed
   */
  public function testDuplicateRegistrantsAllowedNoField() {
    $field = FieldConfig::loadByName('entity_test', 'entity_test', EventManagerInterface::FIELD_ALLOW_DUPLICATE_REGISTRANTS);
    $field->delete();

    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertFalse($event_meta->duplicateRegistrantsAllowed());
  }

  /**
   * Tests if a wait list is allowed.
   *
   * @covers ::allowWaitList
   * @dataProvider allowWaitListDataProvider
   */
  public function testAllowWaitList($expected, $values) {
    $event = $this->createEvent($values);
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertSame($expected, $event_meta->allowWaitList());
  }

  /**
   * Data provider for ::testIsAcceptingRegistrations().
   *
   * @return array
   *   Test cases.
   */
  public function allowWaitListDataProvider(): array {
    return [
      'enabled' => [
        'expected' => TRUE,
        'values' => [EventManagerInterface::FIELD_WAIT_LIST => TRUE],
      ],
      'disabled' => [
        'expected' => FALSE,
        'values' => [EventManagerInterface::FIELD_WAIT_LIST => FALSE],
      ],
    ];
  }

  /**
   * Tests if a wait list is allowed.
   *
   * @covers ::allowWaitList
   * @dataProvider allowWaitListDataProvider
   */
  public function testAllowWaitListNoField() {
    $field = FieldConfig::loadByName('entity_test', 'entity_test', EventManagerInterface::FIELD_WAIT_LIST);
    $field->delete();

    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertFalse($event_meta->allowWaitList());
  }

  /**
   * Tests getting a list of allowed registration type ID's.
   *
   * @covers ::getRegistrationTypeIds
   */
  public function testGetRegistrationTypeIds() {
    $event = $this->createEvent([
      EventManagerInterface::FIELD_REGISTRATION_TYPE => 'registration_type_a',
    ]);
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertSame(['registration_type_a'], $event_meta->getRegistrationTypeIds());
  }

  /**
   * Tests getting registration type ID's when there is no field.
   *
   * @covers ::getRegistrationTypeIds
   */
  public function testGetRegistrationTypeIdsNoField() {
    $field = FieldConfig::loadByName('entity_test', 'entity_test', EventManagerInterface::FIELD_REGISTRATION_TYPE);
    $field->delete();

    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertSame([], $event_meta->getRegistrationTypeIds());
  }

  /**
   * Tests getting a list of allowed registration types.
   *
   * @covers ::getRegistrationTypes
   */
  public function testGetRegistrationTypes() {
    $event = $this->createEvent([
      EventManagerInterface::FIELD_REGISTRATION_TYPE => 'registration_type_a',
    ]);
    $event_meta = $this->eventManager->getMeta($event);
    $expected = [
      'registration_type_a' => $this->registrationType,
    ];
    $this->assertEquals($expected, $event_meta->getRegistrationTypes());
  }

  /**
   * Tests getting a list of allowed registration types when there is no field.
   *
   * @covers ::getRegistrationTypes
   */
  public function testGetRegistrationTypesNoField() {
    $field = FieldConfig::loadByName('entity_test', 'entity_test', EventManagerInterface::FIELD_REGISTRATION_TYPE);
    $field->delete();

    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertSame([], $event_meta->getRegistrationTypes());
  }

  /**
   * Tests if a registration type is allowed to be used on an event.
   *
   * @covers ::registrationTypeIsValid
   */
  public function testRegistrationTypeIsValid() {
    $event = $this->createEvent([
      EventManagerInterface::FIELD_REGISTRATION_TYPE => 'registration_type_a',
    ]);
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertTrue($event_meta->registrationTypeIsValid($this->registrationType));
  }

  /**
   * Tests if a registration type is NOT allowed to be used on an event.
   *
   * @covers ::registrationTypeIsValid
   */
  public function testRegistrationTypeIsValidNotSet() {
    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertFalse($event_meta->registrationTypeIsValid($this->registrationType));
  }

  /**
   * Tests if an allowed registration type can be removed from an event.
   *
   * @covers ::removeRegistrationType
   */
  public function testRemoveRegistrationType() {
    $event = $this->createEvent([
      EventManagerInterface::FIELD_REGISTRATION_TYPE => 'registration_type_a',
    ]);
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertSame($event, $event_meta->removeRegistrationType('registration_type_a'));
    $this->assertSame([], $event_meta->getRegistrationTypes());
  }

  /**
   * Tests if a registration type is removed in case it is referenced twice.
   *
   * @covers ::removeRegistrationType
   */
  public function testRemoveMultipleRegistrationTypes() {
    $event = $this->createEvent([
      EventManagerInterface::FIELD_REGISTRATION_TYPE => [
        'registration_type_a',
        'registration_type_a',
      ],
    ]);
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertSame($event, $event_meta->removeRegistrationType('registration_type_a'));
    $this->assertSame([], $event_meta->getRegistrationTypes());
  }

  /**
   * Tests if capacity gets returned.
   *
   * @covers ::getRegistrantCapacity
   */
  public function testGetRegistrantCapacity() {
    $event = $this->createEvent([
      EventManagerInterface::FIELD_REGISTRANTS_CAPACITY => 4,
    ]);
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertSame(4, $event_meta->getRegistrantCapacity());
  }

  /**
   * Tests if capacity gets returned when there is no field.
   *
   * @covers ::getRegistrantCapacity
   */
  public function testGetRegistrantCapacityNoField() {
    $field = FieldConfig::loadByName('entity_test', 'entity_test', EventManagerInterface::FIELD_REGISTRANTS_CAPACITY);
    $field->delete();

    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertSame(EventMetaInterface::CAPACITY_UNLIMITED, $event_meta->getRegistrantCapacity());
  }

  /**
   * Tests remaining capacity with one registrant and a capacity set.
   *
   * @covers ::remainingRegistrantCapacity
   */
  public function testRemainingRegistrantCapacity() {
    // Set capacity to 4.
    $event = $this->createEvent([
      EventManagerInterface::FIELD_REGISTRANTS_CAPACITY => 4,
    ]);

    // Register a single registrant.
    $user1 = $this->drupalCreateUser();
    $this->createRegistration($event, $this->registrationType, [$user1]);

    $event_meta = $this->eventManager->getMeta($event);
    $this->assertSame(3, $event_meta->remainingRegistrantCapacity());
  }

  /**
   * Tests that there is capacity when there are no registrants yet.
   *
   * @covers ::hasCapacity
   */
  public function testHasCapacity() {
    // Set capacity to 4.
    $event = $this->createEvent([
      EventManagerInterface::FIELD_REGISTRANTS_CAPACITY => 4,
    ]);
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertTrue($event_meta->hasCapacity());
  }

  /**
   * Tests that there is capacity when there is one registrant.
   *
   * @covers ::hasCapacity
   */
  public function testHasCapacityWithOneRegistrantAndFourCapacity() {
    // Set capacity to 4.
    $event = $this->createEvent([
      EventManagerInterface::FIELD_REGISTRANTS_CAPACITY => 4,
    ]);

    // Register a single registrant.
    $user1 = $this->drupalCreateUser();
    $this->createRegistration($event, $this->registrationType, [$user1]);

    $event_meta = $this->eventManager->getMeta($event);
    $this->assertTrue($event_meta->hasCapacity());
  }

  /**
   * Tests no capacity remaining with one registrant and one capacity.
   *
   * @covers ::hasCapacity
   */
  public function testHasCapacityWithOneRegistrantAndOneCapacity() {
    // Set capacity to 1.
    $event = $this->createEvent([
      EventManagerInterface::FIELD_REGISTRANTS_CAPACITY => 1,
    ]);

    // Register a single registrant.
    $user1 = $this->drupalCreateUser();
    $this->createRegistration($event, $this->registrationType, [$user1]);

    $event_meta = $this->eventManager->getMeta($event);
    $this->assertFalse($event_meta->hasCapacity());
  }

  /**
   * Tests no capacity remaining with two registrants and one capacity.
   *
   * @covers ::hasCapacity
   */
  public function testHasCapacityWithTwoRegistrantsAndOneCapacity() {
    // Set capacity to 1.
    $event = $this->createEvent([
      EventManagerInterface::FIELD_REGISTRANTS_CAPACITY => 1,
    ]);

    // Register two registrants.
    $user1 = $this->drupalCreateUser();
    $user2 = $this->drupalCreateUser();
    $this->createRegistration($event, $this->registrationType, [$user1, $user2]);

    $event_meta = $this->eventManager->getMeta($event);
    $this->assertFalse($event_meta->hasCapacity());
  }

  /**
   * Tests that there is capacity if the capacity is set to unlimited.
   *
   * @covers ::hasCapacity
   */
  public function testHasCapacityWhenCapacityIsUnlimited() {
    // Set capacity to unlimited.
    $event = $this->createEvent([
      EventManagerInterface::FIELD_REGISTRANTS_CAPACITY => EventMetaInterface::CAPACITY_UNLIMITED,
    ]);

    // Register a single registrant.
    $user1 = $this->drupalCreateUser();
    $this->createRegistration($event, $this->registrationType, [$user1]);

    $event_meta = $this->eventManager->getMeta($event);
    $this->assertTrue($event_meta->hasCapacity());
  }

  /**
   * Tests maximum registrants is unlimited if there is no field value.
   *
   * Including no default field value on the entity level.
   *
   * @covers ::getRegistrantsMaximum
   */
  public function testRegistrantsMaximumNoField() {
    $field = FieldConfig::loadByName('entity_test', 'entity_test', EventManagerInterface::FIELD_REGISTRANTS_CAPACITY);
    $field->delete();

    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertSame($this->unlimited, $event_meta->getRegistrantsMaximum(), 'Maximum registrants is unlimited when no field exists.');
  }

  /**
   * Tests maximum registrants is unlimited if there is no field value.
   *
   * @covers ::getRegistrantsMaximum
   */
  public function testRegistrantsMaximumDefaultValue() {
    $field = FieldConfig::loadByName('entity_test', 'entity_test', EventManagerInterface::FIELD_REGISTRANTS_CAPACITY);
    $field
      ->setDefaultValue([['value' => 666]])
      ->save();

    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertSame(666, $event_meta->getRegistrantsMaximum(), 'Maximum registrants matches bundle default value.');
  }

  /**
   * Tests maximum registrants is unlimited if there is no field value.
   *
   * @covers ::getRegistrantsMaximum
   */
  public function testRegistrantsMaximumNoDefaultValue() {
    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertSame($this->unlimited, $event_meta->getRegistrantsMaximum(), 'Maximum registrants matches empty bundle default.');
  }

  /**
   * Tests if the code can detect that an event is in the past.
   *
   * @covers ::isPastEvent
   */
  public function testIsPastEventWhenDateIsInPast() {
    // @todo This test is incomplete.
    $this->markTestIncomplete();

    // Add date fields to the event type.
    // @todo ^

    // Create an event with a date in the past.
    // @todo ^
    $event = $this->createEvent([
      'field_start_date' => '',
      'field_end_date' => '',
    ]);

    $event_meta = $this->eventManager->getMeta($event);
    $this->assertTrue($event_meta->isPastEvent());
  }

  /**
   * Tests if the code can detect that an event is in the future.
   *
   * @covers ::isPastEvent
   */
  public function testIsPastEventWhenDateIsInFuture() {
    // @todo This test is incomplete.
    $this->markTestIncomplete();

    // Add date fields to the event type.
    // @todo ^

    // Create an event with a date in the future.
    // @todo ^
    $event = $this->createEvent([
      'field_start_date' => '',
      'field_end_date' => '',
    ]);

    $event_meta = $this->eventManager->getMeta($event);
    $this->assertFalse($event_meta->isPastEvent());
  }

  /**
   * Tests that an exception is thrown if no date fields are configured.
   *
   * @covers ::isPastEvent
   */
  public function testIsPastEventNoField() {
    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->expectException(\RuntimeException::class);
    $event_meta->isPastEvent();
  }

  /**
   * @covers ::getDateString
   */
  public function testGetDateString() {
    // @todo This test is incomplete.
    $this->markTestIncomplete();

    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertNull($event_meta->getDateString());
  }

  /**
   * Tests that method returns nothing if no date fields are configured.
   *
   * @covers ::getDateString
   */
  public function testGetDateStringNoField() {
    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertNull($event_meta->getDateString());
  }

  /**
   * @covers ::buildQuery
   */
  public function testBuildQuery() {
    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertInstanceOf(QueryInterface::class, $event_meta->buildQuery('registration'));
  }

  /**
   * @covers ::buildRegistrationQuery
   */
  public function testBuildRegistrationQuery() {
    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertInstanceOf(QueryInterface::class, $event_meta->buildRegistrationQuery());
  }

  /**
   * @covers ::buildRegistrantQuery
   */
  public function testBuildRegistrantQuery() {
    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertInstanceOf(QueryInterface::class, $event_meta->buildRegistrantQuery());
  }

  /**
   * @covers ::buildEventRegistrantQuery
   */
  public function testBuildEventRegistrantQuery() {
    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertInstanceOf(SelectInterface::class, $event_meta->buildEventRegistrantQuery());
  }

  /**
   * @covers ::getRegistrations
   */
  public function testGetRegistrations() {
    // Create a few events.
    $event1 = $this->createEvent();
    $event2 = $this->createEvent();
    $event3 = $this->createEvent();

    // Create users.
    $user1 = $this->drupalCreateUser();
    $user2 = $this->drupalCreateUser();
    $user3 = $this->drupalCreateUser();

    // Create a few registrations for the first two events.
    $event1_registration = $this->createRegistration($event1, $this->registrationType, [$user1]);
    $event2_registration1 = $this->createRegistration($event2, $this->registrationType, [$user2]);
    $event2_registration2 = $this->createRegistration($event2, $this->registrationType, [$user3]);

    // Check that for event 1 only the registrations belonging to that event
    // are returned.
    $event_meta1 = $this->eventManager->getMeta($event1);
    $expected_registration_ids = [$event1_registration->id()];
    $registration_ids = [];
    foreach ($event_meta1->getRegistrations() as $registration) {
      $this->assertInstanceof(RegistrationInterface::class, $registration);
      $registration_ids[] = $registration->id();
    }
    $this->assertSame($expected_registration_ids, $registration_ids);

    // Check for event 2 too.
    $event_meta2 = $this->eventManager->getMeta($event2);
    $expected_registration_ids = [
      $event2_registration1->id(),
      $event2_registration2->id(),
    ];
    $registration_ids = [];
    foreach ($event_meta2->getRegistrations() as $registration) {
      $this->assertInstanceof(RegistrationInterface::class, $registration);
      $registration_ids[] = $registration->id();
    }
    $this->assertSame($expected_registration_ids, $registration_ids);

    // And for event 3 (no registrations expected).
    $event_meta3 = $this->eventManager->getMeta($event3);
    $this->assertSame([], $event_meta3->getRegistrations());
  }

  /**
   * @covers ::countRegistrants
   */
  public function testCountRegistrations() {
    // Create a few events.
    $event1 = $this->createEvent();
    $event2 = $this->createEvent();
    $event3 = $this->createEvent();

    // Create users.
    $user1 = $this->drupalCreateUser();
    $user2 = $this->drupalCreateUser();
    $user3 = $this->drupalCreateUser();

    // Create a few registrations for the first two events.
    $this->createRegistration($event1, $this->registrationType, [$user1]);
    $this->createRegistration($event2, $this->registrationType, [$user2]);
    $this->createRegistration($event2, $this->registrationType, [$user3]);

    // Check that for event 1 there is one registration.
    $event_meta1 = $this->eventManager->getMeta($event1);
    $this->assertSame(1, $event_meta1->countRegistrations());

    // Check that for event 2 there are two registrations.
    $event_meta2 = $this->eventManager->getMeta($event2);
    $this->assertSame(2, $event_meta2->countRegistrations());

    // Check that for event 3 there a no registrations.
    $event_meta3 = $this->eventManager->getMeta($event3);
    $this->assertSame(0, $event_meta3->countRegistrations());
  }

  /**
   * @covers ::getRegistrants
   */
  public function testGetRegistrants() {
    // Create a few events.
    $event1 = $this->createEvent();
    $event2 = $this->createEvent();
    $event3 = $this->createEvent();

    // Create a registration with two registrants for event 1.
    $user1 = $this->drupalCreateUser();
    $user2 = $this->drupalCreateUser();
    $this->createRegistration($event1, $this->registrationType, [$user1, $user2]);

    // Create two registrations for event 2.
    $user3 = $this->drupalCreateUser();
    $this->createRegistration($event2, $this->registrationType, [$user3]);
    $user4 = $this->drupalCreateUser();
    $user5 = $this->drupalCreateUser();
    $this->createRegistration($event2, $this->registrationType, [$user4, $user5]);

    // Check the expected registrants for event 1.
    $expected1 = [
      [
        'entity_type' => 'user',
        'entity_id' => $user1->id(),
      ],
      [
        'entity_type' => 'user',
        'entity_id' => $user2->id(),
      ],
    ];
    $event_meta1 = $this->eventManager->getMeta($event1);
    $registrants1 = $event_meta1->getRegistrants();
    // Collect registrants.
    $identity_ids = [];
    foreach ($registrants1 as $registrant) {
      $this->assertInstanceof(RegistrantInterface::class, $registrant);
      $identity_ids[] = $registrant->getIdentityId();
    }
    $this->assertEquals($expected1, $identity_ids);

    // Check the expected registrants for event 2.
    $expected2 = [
      [
        'entity_type' => 'user',
        'entity_id' => $user3->id(),
      ],
      [
        'entity_type' => 'user',
        'entity_id' => $user4->id(),
      ],
      [
        'entity_type' => 'user',
        'entity_id' => $user5->id(),
      ],
    ];
    $event_meta2 = $this->eventManager->getMeta($event2);
    $registrants2 = $event_meta2->getRegistrants();
    // Collect registrants.
    $identity_ids = [];
    foreach ($registrants2 as $registrant) {
      $this->assertInstanceof(RegistrantInterface::class, $registrant);
      $identity_ids[] = $registrant->getIdentityId();
    }
    $this->assertEquals($expected2, $identity_ids);

    // Check the expected registrants for event 3.
    $event_meta3 = $this->eventManager->getMeta($event3);
    $this->assertSame([], $event_meta3->getRegistrants());
  }

  /**
   * @covers ::countRegistrations
   */
  public function testCountRegistrants() {
    // Create a few events.
    $event1 = $this->createEvent();
    $event2 = $this->createEvent();
    $event3 = $this->createEvent();

    // Create a registration with two registrants for event 1.
    $user1 = $this->drupalCreateUser();
    $user2 = $this->drupalCreateUser();
    $this->createRegistration($event1, $this->registrationType, [$user1, $user2]);

    // Create two registrations for event 2.
    $user3 = $this->drupalCreateUser();
    $this->createRegistration($event2, $this->registrationType, [$user3]);
    $user4 = $this->drupalCreateUser();
    $user5 = $this->drupalCreateUser();
    $this->createRegistration($event2, $this->registrationType, [$user4, $user5]);

    // Check the events for the expected amount of registrants.
    $event_meta1 = $this->eventManager->getMeta($event1);
    $this->assertSame(2, $event_meta1->countRegistrants());
    $event_meta2 = $this->eventManager->getMeta($event2);
    $this->assertSame(3, $event_meta2->countRegistrants());
    $event_meta3 = $this->eventManager->getMeta($event3);
    $this->assertSame(0, $event_meta3->countRegistrants());
  }

  /**
   * @covers ::canRegisterProxyIdentities
   */
  public function testCanRegisterProxyIdentities() {
    // @todo This test is incomplete.
    $this->markTestIncomplete();

    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertNull($event_meta->canRegisterProxyIdentities());
  }

  /**
   * @covers ::countProxyIdentities
   */
  public function testCountProxyIdentities() {
    // @todo This test is incomplete.
    $this->markTestIncomplete();

    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertNull($event_meta->countProxyIdentities());
  }

  /**
   * @covers ::getIdentityTypes
   */
  public function testGetIdentityTypes() {
    // @todo This test is incomplete.
    $this->markTestIncomplete();

    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertNull($event_meta->getIdentityTypes());
  }

  /**
   * @covers ::getCreatableIdentityTypes
   */
  public function testGetCreatableIdentityTypes() {
    // @todo This test is incomplete.
    $this->markTestIncomplete();

    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertNull($event_meta->getCreatableIdentityTypes());
  }

  /**
   * @covers ::identitiesCanRegister
   */
  public function testIdentitiesCanRegister() {
    // @todo This test is incomplete.
    $this->markTestIncomplete();

    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertNull($event_meta->identitiesCanRegister());
  }

  /**
   * @covers ::getGroups
   */
  public function testGetGroups() {
    // @todo This test is incomplete.
    $this->markTestIncomplete();

    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertNull($event_meta->getGroups());
  }

  /**
   * Testing get default groups if there are none.
   *
   * @covers ::getDefaultGroups
   */
  public function testGetDefaultGroupsWhenNone() {
    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertSame([], $event_meta->getDefaultGroups());
  }

  /**
   * Testing get default groups if groups exist.
   *
   * @covers ::getDefaultGroups
   */
  public function testGetDefaultGroups() {
    // @todo This test is incomplete.
    $this->markTestIncomplete();

    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertSame([], $event_meta->getDefaultGroups());
  }

  /**
   * Testing get default groups if groups field does not exist.
   *
   * @covers ::getDefaultGroups
   */
  public function testGetDefaultGroupsNoField() {
    $field = FieldConfig::loadByName('entity_test', 'entity_test', EventManagerInterface::FIELD_REGISTRATION_GROUPS);
    $field->delete();

    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertSame([], $event_meta->getDefaultGroups());
  }

  /**
   * Tests if a referenced group can be removed from an event.
   *
   * @covers ::removeGroup
   */
  public function testRemoveGroup() {
    // @todo This test is incomplete.
    $this->markTestIncomplete();

    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertSame($event, $event_meta->removeGroup());
  }

  /**
   * @covers ::buildGroupQuery
   */
  public function testBuildGroupQuery() {
    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertInstanceOf(QueryInterface::class, $event_meta->buildGroupQuery());
  }

  /**
   * @covers ::getRules
   */
  public function testGetRules() {
    // @todo This test is incomplete.
    $this->markTestIncomplete();

    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertNull($event_meta->getRules());
  }

  /**
   * @covers ::getDefaultRules
   */
  public function testGetDefaultRules() {
    // @todo This test is incomplete.
    $this->markTestIncomplete();

    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertNull($event_meta->getDefaultRules());
  }

  /**
   * @covers ::isDefaultRules
   */
  public function testIsDefaultRules() {
    // @todo This test is incomplete.
    $this->markTestIncomplete();

    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertNull($event_meta->isDefaultRules());
  }

  /**
   * @covers ::buildRuleQuery
   */
  public function testBuildRuleQuery() {
    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertInstanceOf(QueryInterface::class, $event_meta->buildRuleQuery());
  }

  /**
   * @covers ::trigger
   */
  public function testTrigger() {
    // @todo This test is incomplete.
    $this->markTestIncomplete();

    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertNull($event_meta->trigger());
  }

  /**
   * @covers ::addDefaultAccess
   */
  public function testAddDefaultAccess() {
    // @todo This test is incomplete.
    $this->markTestIncomplete();

    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertNull($event_meta->addDefaultAccess());
  }

  /**
   * @covers ::createDefaultEventMessages
   */
  public function testCreateDefaultEventMessages() {
    // @todo This test is incomplete.
    $this->markTestIncomplete();

    $event = $this->createEvent();
    $event_meta = $this->eventManager->getMeta($event);
    $this->assertNull($event_meta->createDefaultEventMessages());
  }

  /**
   * Creates an event with the given values and saves it.
   *
   * @param array $values
   *   (optional) The values to set for the event.
   *
   * @return \Drupal\entity_test\Entity\EntityTest
   *   The saved event.
   */
  protected function createEvent(array $values = []): EntityTest {
    $event = EntityTest::create($values);
    $event->save();
    return $event;
  }

}
